import cv2 as cv
import numpy as np
from scipy.fftpack import fftshift, ifft, fft

class Radon(object):

    def __init__(self, in_image):
        self.img = in_image
        w, h = in_image.shape
        self.d = int(np.sqrt(w ** 2 + h ** 2))
        self.dw = int((self.d - w) // 2)
        self.dh = int((self.d - h) // 2)

    def ForwardRadon(self):
        zoom_img = cv.copyMakeBorder(self.img, self.dh, self.dh, self.dw, self.dw, cv.BORDER_CONSTANT)
        h, w = zoom_img.shape
        sinogram_ = np.zeros((self.d, 180))
        for i in range(180):
            rot = cv.getRotationMatrix2D((h//2, w//2), i, 1.0)
            rot_img = cv.warpAffine(zoom_img, rot, (w, h))
            sinogram_[:, i] = np.sum(rot_img, axis=0)
        cv.normalize(sinogram_, sinogram_, 0, 255, cv.NORM_MINMAX)
        return np.asarray(sinogram_, dtype=np.uint8)

    def FilterProjection(self, sinogram):
        size, num_angle = sinogram.shape
        w = np.arange(-size/2, size/2, 1)
        filter = fftshift(np.abs(w))
        sinogram = np.float32(sinogram)
        filter_sinogram = np.zeros((size, num_angle))
        for i in range(num_angle):
            filter_proj = (fft(sinogram[:, i])*filter).T
            filter_sinogram[:, i] = np.real(ifft(filter_proj))
        cv.normalize(filter_sinogram, filter_sinogram, 0, 255, cv.NORM_MINMAX)
        return np.asarray(filter_sinogram, dtype=np.uint8)

    def BackProjection(self, sinogram):
        size, num_angle = sinogram.shape
        rec_img = np.zeros((size, size), np.uint8)
        for i in range(num_angle):
            buf = np.zeros((size, size), np.uint8)
            for j in range(size):
                buf[j, :] = sinogram[:, i]
            cv.normalize(buf, buf, 0, 1, cv.NORM_MINMAX)
            rot = cv.getRotationMatrix2D((size//2, size//2), -i, 1.0)
            r = cv.warpAffine(buf, rot, (size, size))
            rec_img += cv.warpAffine(buf, rot, (size, size))
        cv.normalize(rec_img, rec_img, 0, 255, cv.NORM_MINMAX)
        return rec_img


if __name__ == '__main__':
    img = cv.imread('testCat.jpg', cv.IMREAD_GRAYSCALE)
    radon = Radon(img)
    sinogram = radon.ForwardRadon()
    filter_sinogram = radon.FilterProjection(sinogram)
    back_proj = radon.BackProjection(filter_sinogram)
    cv.imshow('image', img)
    cv.imshow('sinogram', sinogram)
    cv.imshow('back_projection', back_proj)
    cv.waitKey(100000)

